/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.pentaho.det.di.plugin.DetMenuPlugin;
import org.pentaho.det.di.api.services.ITransGraphService;

@Path( "transgraph" )
public class RestTransGraphService {

  // region Properties
  public ITransGraphService getTransGraphService() {
    return this.transGraphService;
  }
  private ITransGraphService transGraphService;

  public DetMenuPlugin getDetPlugin() {
    return this.detPlugin;
  }
  private DetMenuPlugin detPlugin;
  // endergion

  // region Constructors
  public RestTransGraphService( ITransGraphService transGraphService ) {
    this.transGraphService = transGraphService;
  }
  public RestTransGraphService( DetMenuPlugin plugin ) {
    this.detPlugin = plugin;
  }
  public RestTransGraphService( ITransGraphService transGraphService, DetMenuPlugin plugin ) {
    this.transGraphService = transGraphService;
    this.detPlugin = plugin;
  }
  // endregion

  /***
   * Stops running the transformation
   * @param transGraphId
   * @return
   */
  @GET
  @Produces( MediaType.APPLICATION_JSON )
  @Path( "/{transGraphId}/stop" )
  public Response stop( @PathParam( "transGraphId" ) String transGraphId ) {

    this.getTransGraphService().stop( transGraphId );
    return Response.ok().build();
  }

  @GET
  @Produces( MediaType.APPLICATION_JSON )
  @Path( "/{transGraphId}/resume" )
  public Response resume( @PathParam( "transGraphId" ) String transGraphId ) {

    this.getTransGraphService().resume( transGraphId );
    return Response.ok().build();
  }

}
