/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.services;

import org.pentaho.det.di.api.services.ITransGraphService;
import org.pentaho.di.core.Condition;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.logging.KettleLogStore;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.logging.LoggingRegistry;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransAdapter;
import org.pentaho.di.trans.TransExecutionConfiguration;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.debug.BreakPointListener;
import org.pentaho.di.trans.debug.StepDebugMeta;
import org.pentaho.di.trans.debug.TransDebugMeta;
import org.pentaho.di.trans.debug.TransDebugMetaWrapper;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.ui.spoon.SpoonUiExtenderPluginInterface;
import org.pentaho.di.ui.spoon.SpoonUiExtenderPluginType;
import org.pentaho.xul.swt.tab.TabItem;
import org.pentaho.di.ui.spoon.Spoon;
import org.pentaho.di.ui.spoon.TabMapEntry;
import org.pentaho.di.ui.spoon.delegates.SpoonDelegates;
import org.pentaho.di.ui.spoon.delegates.SpoonTabsDelegate;
import org.pentaho.di.ui.spoon.trans.TransGraph;

import java.util.List;

public class TransGraphService implements ITransGraphService {

  // region Properties
  // TODO: verify if dependency can't be set to more specific objects (e.g. SpoonTabDelegates)
  public Spoon getSpoon() {
    return Spoon.getInstance();
  }
  private Spoon spoon;
  // endregion

  // region Constructors
  public TransGraphService() {
    //this.spoon = Spoon.getInstance();
  }

  public TransGraphService( Spoon spoon ) {
    this.spoon = spoon;
  }

  public TransDebugMeta getTransDebugMeta() {
    return this.transDebugMeta;
  }
  private TransDebugMeta transDebugMeta;
  // endregion

  // region ITransGraphService
  @Override
  /**
   * Stops the transformation running in the transGraph with the given id
   *
   * @param transGraphId Id of the transGraph being previewed by DET
   *
   */
  public synchronized void stop( String transGraphId ) {
    TransGraph transGraph = this.getTransGraph( transGraphId );
    if ( transGraph == null ) {
      return;
    }
    transGraph.stop();
  }

  @Override
  /**
   * Resumes the transformation running in the transGraph with the given id.
   * Used to request more rows to preview in DET
   *
   * @param transGraphId Id of the transGraph being previewed by DET
   */
  public synchronized void resume( final String transGraphId ) {
    final TransGraph transGraph = this.getTransGraph( transGraphId );
    if ( transGraph == null ) {
      return;
    }

    final TransGraphService service = this;
    this.getSpoon().getDisplay().asyncExec( new Runnable() {
      @Override
      public void run() {
        service.getMoreRows( transGraph );
      }
    } );
  }

  /**
   * Displays DET in a new tab with the data from the transGraph with given id
   *
   * @param transGraphId Id of the transGraph being previewed by DET
   * @param breakPointListeners List of breakPoint listeners
   */
  public synchronized void showDet( String transGraphId, List<BreakPointListener> breakPointListeners ) {
    Spoon spoon = this.spoon = this.getSpoon();
    LogChannelInterface spoonLog = spoon.getLog();

    TransGraph transGraph = this.getTransGraph( transGraphId );
    TransMeta transMeta = transGraph.getManagedObject();
    TransDebugMeta transDebugMeta = this.transDebugMeta = this.buildTransDebugMeta( transMeta );

    TransExecutionConfiguration executionConfiguration = new TransExecutionConfiguration();

    if ( !transGraph.isRunning() ) {
      try {
        //region configuration
        /*spoonLog.setLogLevel( executionConfiguration.getLogLevel() );
        if ( spoonLog.isDetailed() ) {
            spoonLog.logDetailed( BaseMessages.getString( PKG, "TransLog.Log.DoPreview" ) );
        }*/
        String[] args = new String[ 0 ]; //null;
        /*Map<String, String> arguments = executionConfiguration.getArguments();
        if ( arguments != null ) {
            args = convertArguments( arguments );
        }
        transMeta.injectVariables( executionConfiguration.getVariables() );

        // Set the named parameters
        Map<String, String> paramMap = executionConfiguration.getParams();
        Set<String> keys = paramMap.keySet();
        for ( String key : keys ) {
            transMeta.setParameterValue( key, Const.NVL(paramMap.get(key), "") );
        }

        transMeta.activateParameters();*/

        // Do we need to clear the log before running?
        if ( executionConfiguration.isClearingLog() ) {
          transGraph.transLogDelegate.clearLog();
        }
        //endregion

        //region Trans
        // Do we have a previous execution to clean up in the logging registry?
        if ( transGraph.trans != null ) {
          KettleLogStore.discardLines( transGraph.trans.getLogChannelId(), false );
          LoggingRegistry.getInstance().removeIncludingChildren( transGraph.trans.getLogChannelId() );
        }

        // Create a new transformation to execution
        transGraph.trans = new Trans( transMeta );
        transGraph.trans.setSafeModeEnabled( executionConfiguration.isSafeModeEnabled() );
        transGraph.trans.setPreview( true );
        transGraph.trans.setGatheringMetrics( executionConfiguration.isGatheringMetrics() );
        transGraph.trans.setMetaStore( spoon.getMetaStore() );
        transGraph.trans.prepareExecution( args );
        transGraph.trans.setRepository( spoon.rep );
        //endregion

        List<SpoonUiExtenderPluginInterface> relevantExtenders = SpoonUiExtenderPluginType.getInstance()
          .getRelevantExtenders( TransDebugMetaWrapper.class, TransGraph.PREVIEW_TRANS );

        TransDebugMetaWrapper transDebugMetaWrapper = new TransDebugMetaWrapper( transGraph.trans, transDebugMeta );
        for ( SpoonUiExtenderPluginInterface relevantExtender : relevantExtenders ) {
          relevantExtender.uiEvent( transDebugMetaWrapper, TransGraph.PREVIEW_TRANS );
        }

        // Add the row listeners to the allocated threads
        transDebugMeta.addRowListenersToTransformation( transGraph.trans );

        // What method should we call back when a break-point is hit?
        for ( BreakPointListener breakPoint : breakPointListeners ) {
          transDebugMeta.addBreakPointListers( breakPoint );
        }

        // Do we capture data?
        if ( transGraph.transPreviewDelegate.isActive() ) {
          transGraph.transPreviewDelegate.capturePreviewData( transGraph.trans, transMeta.getSteps() );
        }

        // Start the threads for the steps...
        this.startThreads( transGraph );

      } catch ( Exception e ) {
        spoonLog.logError( "Well this is error!!" );
      }
    }
  }
  // endregion

  public synchronized void getMoreRows( TransGraph transGraph ) {
    TransDebugMeta transDebugMeta = this.getTransDebugMeta();

    for ( StepDebugMeta stepDebugMeta : transDebugMeta.getStepDebugMetaMap().values() ) {
      List<Object[]> rowBuffer = stepDebugMeta.getRowBuffer();
      Integer previewRows = stepDebugMeta.getRowCount();

      if ( previewRows.equals( rowBuffer.size() ) ) {
        rowBuffer.clear();
      }
    }

    transGraph.pauseTransformation();
  }

  //region Auxiliary Methods
  protected TransGraph getTransGraph( String transGraphId ) {
    TabMapEntry mapEntry = this.getTabMapEntry( transGraphId, TabMapEntry.ObjectType.TRANSFORMATION_GRAPH );
    if ( mapEntry == null
      || !( mapEntry.getObject() instanceof TransGraph ) ) {
      return null;
    }

    return (TransGraph) mapEntry.getObject();
  }

  private TabMapEntry getTabMapEntry( String tabItemId, TabMapEntry.ObjectType objectType ) {
    SpoonTabsDelegate tabsDelegate = this.getTabDelegate();
    if ( tabsDelegate == null ) {
      return null;
    }

    for ( TabMapEntry tabMapEntry : tabsDelegate.getTabs() ) {
      TabItem tabItem = tabMapEntry.getTabItem();
      if ( tabItem.isDisposed() ) {
        continue;
      }
      if ( tabItem.getId().equalsIgnoreCase( tabItemId )
        && tabMapEntry.getObjectType().equals( objectType ) ) {
        return tabMapEntry;
      }
    }

    return null;
  }

  private SpoonTabsDelegate getTabDelegate() {
    SpoonDelegates delegates = this.getSpoon().delegates;
    if ( delegates == null ) {
      return null;
    }

    return delegates.tabs;
  }

  private TransDebugMeta buildTransDebugMeta( TransMeta transMeta ) {
    //creating TransDebugMeta
    TransDebugMeta transDebugMeta = new TransDebugMeta( transMeta );

    //creating and config StepDebugMeta
    for ( StepMeta stepMeta : transMeta.getSelectedSteps() ) {
      StepDebugMeta stepDebugMeta = new StepDebugMeta( stepMeta );

      stepDebugMeta.setCondition( new Condition() );
      stepDebugMeta.setPausingOnBreakPoint( false );
      stepDebugMeta.setReadingFirstRows( true );
      stepDebugMeta.setRowCount( 999 );

      transDebugMeta.getStepDebugMetaMap().put( stepMeta, stepDebugMeta );
    }

    return transDebugMeta;
  }

  private void startThreads( final TransGraph activeTG ) {
    activeTG.setRunning( true );
    try {
      // Add a listener to the transformation.
      // If the transformation is done, we want to do the end processing, etc.
      activeTG.trans.addTransListener( new TransAdapter() {
        public void transFinished( Trans trans ) {
          //activeTG.checkTransEnded();
          //activeTG.checkErrorVisuals();
          //activeTG.stopRedrawTimer();

          activeTG.transMetricsDelegate.resetLastRefreshTime();
          activeTG.transMetricsDelegate.updateGraph();
        }
      } );

      activeTG.trans.startThreads();
      //activeTG.startRedrawTimer();

      activeTG.setControlStates();
    } catch ( KettleException e ) {
      //log.logError( "Error starting step threads", e );
      //activeTG.checkErrorVisuals();
      //activeTG.stopRedrawTimer();
    }
  }
  //endregion
}
