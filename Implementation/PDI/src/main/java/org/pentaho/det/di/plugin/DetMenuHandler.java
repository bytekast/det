/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.det.di.services.TransGraphService;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.debug.BreakPointListener;
import org.pentaho.di.trans.debug.StepDebugMeta;
import org.pentaho.di.trans.debug.TransDebugMeta;
import org.pentaho.di.ui.spoon.Spoon;
import org.pentaho.di.ui.spoon.trans.TransGraph;
import org.pentaho.ui.xul.impl.AbstractXulEventHandler;

import java.util.ArrayList;
import java.util.List;
import java.net.MalformedURLException;
import java.net.URL;


public class DetMenuHandler extends AbstractXulEventHandler {

  // region Properties
  public static final String DET_MENU_EVENT_HANDLER = "detMenuEventHandler";
  public static final String WEB_CLIENT_PATH =
    "/DataExplorerTool/web/index.html#/det-app/det-data-explorer-plugin?datasource-type=pdi-datasource";

  public Spoon getSpoon() {
    return Spoon.getInstance();
  }

  protected Log getLogger() {
    return this.logger;
  }
  private Log logger = LogFactory.getLog( this.getClass() );

  public TransGraphService getTransGraphService() {
    return this.transGraphService;
  }
  private TransGraphService transGraphService;
  //endregion

  // region Constructors
  public DetMenuHandler() { }

  public DetMenuHandler( TransGraphService service ) {
    this.setName( DET_MENU_EVENT_HANDLER );
    this.transGraphService = service;
  }
  // endregion

  //region Methods

  /**
   * Function that defines what det will do when it gets to a breakPoint. Currently it is displaying a new tab in PDI
   * with DET in it.
   */
  public void detExplore() {
    List<BreakPointListener> breakPointListeners = new ArrayList<>();

    breakPointListeners.add( new BreakPointListener() {
      public void breakPointHit( TransDebugMeta transDebugMeta, StepDebugMeta stepDebugMeta,
                                 RowMetaInterface rowBufferMeta, List<Object[]> rowBuffer ) {
        showDetClient();
      }
    } );
    this.getTransGraphService().showDet( this.getActiveTransGraphId(), breakPointListeners );
  }

  /**
   * Function that creates a new tab in PDI to display DET
   */
  public void showDetClient() {
    try {
      final URL url = new URL( getDetUrl() );
      final Spoon spoon = this.getSpoon();
      final TransGraph activeTG = spoon.getActiveTransGraph();

      spoon.getDisplay().asyncExec( new Runnable() {
        public void run() {
          activeTG.pauseTransformation();
          spoon.addSpoonBrowser( getDetTabLabel(), url.toString(), false );
        }
      } );

    } catch ( MalformedURLException e ) {
      this.getLogger().error( "Error on det URL: " + WEB_CLIENT_PATH, e );
    }
  }
  //endregion

  //region Aux Methods
  protected String getActiveTransGraphId() {
    return this.getSpoon().getActiveTabText();
  }

  private String getDetUrl() {
    String transGraphId = this.getActiveTransGraphId();
    String transGraphIdQueryParam = "&transGraphId=" + transGraphId;

    String hostPlatformQueryParam = "&hostPlatform=pdi";

    return "http://localhost:9050" + WEB_CLIENT_PATH + transGraphIdQueryParam + hostPlatformQueryParam;
  }

  private String getDetTabLabel() {
    String id = this.getActiveTransGraphId();
    if ( !id.startsWith( "Explore-" ) ) {
      id = "Explore-" + id;
    }
    return id;
  }
  //endregion
}
