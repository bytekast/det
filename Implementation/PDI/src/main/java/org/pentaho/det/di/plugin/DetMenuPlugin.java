/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.plugin;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.pentaho.det.di.services.TransGraphService;
import org.pentaho.di.ui.spoon.Spoon;
import org.pentaho.di.ui.spoon.SpoonLifecycleListener;
import org.pentaho.di.ui.spoon.SpoonPerspective;
import org.pentaho.di.ui.spoon.SpoonPlugin;
import org.pentaho.di.ui.spoon.SpoonPluginCategories;
import org.pentaho.di.ui.spoon.SpoonPluginInterface;
import org.pentaho.ui.xul.XulDomContainer;
import org.pentaho.ui.xul.XulException;

@SpoonPlugin( id = "detMenuPlugin", image = "" )
@SpoonPluginCategories( { "trans-graph" } )
public class DetMenuPlugin implements SpoonPluginInterface {

  private XulDomContainer container;

  private static final String RESOURCE_BASE = "org/pentaho/det/di/plugin/res";
  private static final String DET_OVERLAY_PATH = RESOURCE_BASE + "/det-overlay.xul";
  private static final String CATEGORY = "trans-graph";


  public void setMenuHandler( DetMenuHandler handler ) {
    this.menuHandler = handler;
  }
  public DetMenuHandler getMenuHandler() {
    return this.menuHandler;
  }
  private DetMenuHandler menuHandler;

  protected Log getLogger() {
    return this.logger;
  }
  private Log logger = LogFactory.getLog( this.getClass() );

  public Spoon getSpoon() {
    return Spoon.getInstance();
  }

  //region constructors
  public DetMenuPlugin() {
    //this.setMenuHandler( new DetMenuHandler() );
  }
  public DetMenuPlugin( TransGraphService service ) {
    this.setMenuHandler( new DetMenuHandler( service ) );
  }
  //endregion


  @Override
  public SpoonLifecycleListener getLifecycleListener() {
    return null;
  }

  @Override
  public SpoonPerspective getPerspective() {
    return null;
  }

  @Override
  public void applyToContainer( String category, XulDomContainer container )
    throws XulException {
    if ( category.equals( CATEGORY ) ) {
      this.container = container;
      container.registerClassLoader( getClass().getClassLoader() );
      container.loadOverlay( DET_OVERLAY_PATH );
      container.addEventHandler( this.getMenuHandler() );
    }
  }

  // Called by OSGI on remove
  public void removeFromContainer() throws XulException {
    this.getLogger().info( "DET Menu Plugin - remove from container!!" );
    if ( container == null ) {
      return;
    }

    final Spoon spoon = this.getSpoon();
    final Log logger = this.getLogger();
    final String menuHandlerName = this.getMenuHandler().getName();
    spoon.getDisplay().syncExec( new Runnable() {
      public void run() {
        try {
          container.removeOverlay( DET_OVERLAY_PATH );
        } catch ( XulException e ) {
          logger.error( "Error removing overlay: " + DET_OVERLAY_PATH, e );
        }
        container.getEventHandlers().remove( menuHandlerName );
        container.deRegisterClassLoader( DetMenuPlugin.class.getClassLoader() );

        // refresh menus
        spoon.enableMenus();
      }
    } );
  }
}
