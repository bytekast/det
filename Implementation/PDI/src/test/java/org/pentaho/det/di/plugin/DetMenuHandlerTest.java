/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.plugin;


import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.pentaho.det.di.services.TransGraphService;
import org.pentaho.di.trans.debug.BreakPointListener;
import org.pentaho.di.ui.spoon.Spoon;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;

public class DetMenuHandlerTest {

  @Test
  public void testDetExplore() {
    String id = "test_det";
    DetMenuHandlerForTest detMenuHandlerForTest = new DetMenuHandlerForTest( id );

    detMenuHandlerForTest.detExplore();
    assertThat( detMenuHandlerForTest.calledShowDet, is( true ) );

  }

  private class DetMenuHandlerForTest extends DetMenuHandler {

    private String activeTransGraphId;
    public boolean calledShowDet;

    public DetMenuHandlerForTest( String id ) {
      this.activeTransGraphId = id;
      this.calledShowDet = false;
    }

    @Override
    protected String getActiveTransGraphId() {
      return this.activeTransGraphId;
    }

    @Override
    public TransGraphService getTransGraphService() {
      final DetMenuHandlerForTest that = this;
      TransGraphService mockService = mock( TransGraphService.class );

      doAnswer( new Answer() {
        @Override
        public String answer( InvocationOnMock invocationOnMock ) throws Throwable {
          Object[] args = invocationOnMock.getArguments();

          String transGraphId = (String) args[0];
          List<BreakPointListener> list = (List<BreakPointListener>) args[1];

          assertThat( transGraphId, is( equalTo( that.getActiveTransGraphId() ) ) );
          assertThat( list, hasSize( 1 ) );

          that.calledShowDet = true;

          return null;
        }
      } ).when( mockService ).showDet( anyString(), anyListOf( BreakPointListener.class ) );

      return mockService;
    }

    @Override
    public Spoon getSpoon() {
      return mock( Spoon.class );
    }
  }
}
