/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.di.service;


import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.pentaho.det.di.services.TransGraphService;
import org.pentaho.di.trans.debug.StepDebugMeta;
import org.pentaho.di.trans.debug.TransDebugMeta;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.ui.spoon.trans.TransGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransGraphServiceTest {

  @Test
  public void testStop() {
    TransGraphServiceForTest transGraphSFT = new TransGraphServiceForTest();
    String transGraphId = "Test_DET";

    transGraphSFT.stop( transGraphId );
    assertThat( transGraphSFT.stopCalled, is( true ) );
  }

  @Test
  public void testGetMoreRows() {
    TransGraphServiceForTest transGraphSFT = new TransGraphServiceForTest();
    String transGraphId = "Test_DET";

    transGraphSFT.getMoreRows( transGraphSFT.getTransGraph( transGraphId ) );
    assertThat( transGraphSFT.resumeCalled, is( true ) );

    TransDebugMeta transDebugMeta = transGraphSFT.getTransDebugMeta();

    for ( StepDebugMeta stepDebugMeta : transDebugMeta.getStepDebugMetaMap().values() ) {
      assertThat( stepDebugMeta.getRowBuffer(), hasSize( 0 ) );
    }
  }

  private class TransGraphServiceForTest extends TransGraphService {
    public boolean stopCalled;
    public boolean resumeCalled;
    public Integer maxRowsPreview;

    public TransGraphServiceForTest() {
      this.stopCalled = false;
      this.resumeCalled = false;
      this.maxRowsPreview = 10;

    }

    @Override
    protected TransGraph getTransGraph( final String transgraphId ) {
      TransGraph transGraph = mock( TransGraph.class );

      doAnswer( new Answer<Object>() {
        @Override
        public Object answer( InvocationOnMock invocationOnMock ) throws Throwable {
          stopCalled = true;
          return null;
        }
      } ).when( transGraph ).stop();

      doAnswer( new Answer<Object>() {
        @Override
        public Object answer( InvocationOnMock invocationOnMock ) throws Throwable {
          resumeCalled = true;
          return null;
        }
      } ).when( transGraph ).pauseTransformation();

      return transGraph;
    }

    @Override
    public TransDebugMeta getTransDebugMeta() {
      TransDebugMeta mockDebugMeta = mock( TransDebugMeta.class );
      StepDebugMeta mockStepDebugMeta = null;

      Map<StepMeta, StepDebugMeta> stepMetaMap = new HashMap<>();

      mockStepDebugMeta = createStepDebugMeta( 10 );
      stepMetaMap.put( mockStepDebugMeta.getStepMeta(), mockStepDebugMeta );

      when( mockDebugMeta.getStepDebugMetaMap() ).thenReturn( stepMetaMap );

      return mockDebugMeta;
    }

    private StepDebugMeta createStepDebugMeta( final int numberOfRows ) {
      StepDebugMeta mockStepDebugMeta = mock( StepDebugMeta.class );
      final Integer maxRows = this.maxRowsPreview;
      final StepMeta mockStepMeta = mock( StepMeta.class );
      final List<Object[]> rowBuffer = new ArrayList<Object[]>();
      for ( int i = 0; i < numberOfRows; i++ ) {
        rowBuffer.add( new Object[0] );
      }

      mockStepDebugMeta.setRowBuffer( rowBuffer );

      when( mockStepDebugMeta.getRowCount() ).then( new Answer<Integer>() {
        @Override
        public Integer answer( InvocationOnMock invocationOnMock ) throws Throwable {
          return maxRows;
        }
      } );

      when( mockStepDebugMeta.getStepMeta() ).then( new Answer<StepMeta>() {
        @Override
        public StepMeta answer( InvocationOnMock invocationOnMock ) throws Throwable {
          return mockStepMeta;
        }
      } );

      return mockStepDebugMeta;
    }
  }
}
