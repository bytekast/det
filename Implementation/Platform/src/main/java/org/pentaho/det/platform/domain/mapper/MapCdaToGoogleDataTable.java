/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.platform.domain.mapper;

import org.pentaho.det.api.domain.IField.ColumnType;
import org.pentaho.det.api.domain.mapper.IConverter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class MapCdaToGoogleDataTable implements IMapCdaToGoogleDataType {

  //region properties
  private static final String CDA_STRING_TYPE = "String";
  private static final String CDA_NUMERIC_TYPE = "Numeric";
  private static final String CDA_DATE_TYPE = "Date";
  private static final String CDA_BOOLEAN_TYPE = "Boolean";

  private Map<String, ColumnType> dataTypeMapping;
  private Map<String, IConverter> converterMapping;
  private static MapCdaToGoogleDataTable instance = null;
  //endregion

  //region Constructors
  private MapCdaToGoogleDataTable() {
    this.dataTypeMapping = defaultTypeMapConfiguration();
    this.converterMapping = defaultConverterConfiguration();
  }
  //endregion

  public static MapCdaToGoogleDataTable getInstance() {
    if ( instance == null ) {
      instance = new MapCdaToGoogleDataTable();
    }
    return instance;
  }

  @Override
  public void putDataType( String cdaType, ColumnType columnType ) {
    this.dataTypeMapping.put( cdaType, columnType );
  }

  @Override
  public void removeDataType( String cdaType ) {
    this.dataTypeMapping.remove( cdaType );
  }

  @Override
  public void clearDataTypeMapping() {
    this.dataTypeMapping.clear();
  }

  @Override
  public ColumnType getDataType( String cdaType ) {
    ColumnType colType = this.dataTypeMapping.get( cdaType );

    if ( colType == null ) {
      colType = ColumnType.STRING;
    }

    return colType;
  }

  @Override
  public Set<Map.Entry<String, ColumnType>> getDataTypes() {
    return this.dataTypeMapping.entrySet();
  }

  @Override
  public void putConverter( String cdaType, IConverter converter ) {
    this.converterMapping.put( cdaType, converter );
  }

  @Override
  public void removeConverter( String cdaType ) {
    this.converterMapping.remove( cdaType );
  }

  @Override
  public void clearConverterMapping() {
    this.converterMapping.clear();
  }

  @Override
  public IConverter getConverter( String cdaType ) {
    return null;
  }

  @Override
  public Set<Map.Entry<String, IConverter>> getConverters() {
    return this.converterMapping.entrySet();
  }

  //region aux methods
  protected Map<String, ColumnType> defaultTypeMapConfiguration() {
    Map<String, ColumnType> defaultConfig = new HashMap<String, ColumnType>();

    defaultConfig.put( MapCdaToGoogleDataTable.CDA_NUMERIC_TYPE,   ColumnType.NUMBER );
    defaultConfig.put( MapCdaToGoogleDataTable.CDA_STRING_TYPE,    ColumnType.STRING );
    defaultConfig.put( MapCdaToGoogleDataTable.CDA_BOOLEAN_TYPE,   ColumnType.BOOLEAN );
    defaultConfig.put( MapCdaToGoogleDataTable.CDA_DATE_TYPE,      ColumnType.DATETIME );

    return defaultConfig;
  }

  protected Map<String, IConverter> defaultConverterConfiguration() {
    Map<String, IConverter> defaultConfig = new HashMap<String, IConverter>();
    return defaultConfig;
  }
  //endregion
}
