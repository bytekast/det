/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.platform;

import org.pentaho.det.api.services.IDataSourceProvider;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;


public class CdaDataSourceProvider implements IDataSourceProvider {


  // region Properties
  @Override
  public Map<UUID, CdaDataSource> getDataSources() {
    return Collections.unmodifiableMap( this.dataSources );
  }
  public CdaDataSourceProvider setDataSources( Map<UUID, CdaDataSource> dataSources ) {
    this.dataSources = dataSources;
    return this;
  }
  private Map<UUID, CdaDataSource> dataSources;

  // endregion

  // region Constructors
  public CdaDataSourceProvider() {
    Map<UUID, CdaDataSource> dataSources = new Hashtable<>();

    String path;
    String dataAccessId;
    CdaDataSource cdaDataSource;

    //First DataSource
    path = "/public/plugin-samples/pentaho-cdf-dd/cde_sample1.cda";
    dataAccessId = "mdxQuery";
    cdaDataSource = new CdaDataSource( path, dataAccessId );
    dataSources.put( cdaDataSource.getUUID(), cdaDataSource );

    //Second DataSource
    path = "/public/plugin-samples/cda/cdafiles/mondrian-jndi.cda";
    dataAccessId = "1";
    cdaDataSource = new CdaDataSource( path, dataAccessId );
    cdaDataSource.putParameter( "status", "Shipped" );
    dataSources.put( cdaDataSource.getUUID(), cdaDataSource );

    //Third DataSource
    path = "/public/plugin-samples/pentaho-cdf-dd/tests/ExportPopup/ExportPopupComponent.cda";
    dataAccessId = "ThreeColumns";
    cdaDataSource = new CdaDataSource( path, dataAccessId );
    dataSources.put( cdaDataSource.getUUID(), cdaDataSource );

    //Fourth DataSource - not SteelWheels
    path = "/public/plugin-samples/pentaho-cdf-dd/tests/FilterComponent/filter_visual_guide.cda";
    dataAccessId = "getData_IdValueGroup";
    cdaDataSource = new CdaDataSource( path, dataAccessId );
    dataSources.put( cdaDataSource.getUUID(), cdaDataSource );

    //Fifth DataSource
    path = "/public/bioMe/det.cda";
    dataAccessId = "BioMe";
    cdaDataSource = new CdaDataSource( path, dataAccessId );
    dataSources.put( cdaDataSource.getUUID(), cdaDataSource );

    this.setDataSources( dataSources );

  }
  // endregion
}
