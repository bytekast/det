/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.platform;

import org.json.JSONObject;
import org.json.JSONArray;

import org.pentaho.det.api.domain.IDataSource;
import org.pentaho.det.api.domain.IDataTable;
import org.pentaho.det.api.domain.IField;
import org.pentaho.det.api.domain.IField.ColumnType;
import org.pentaho.det.domain.DataTable;
import org.pentaho.det.domain.DataTableEntry;
import org.pentaho.det.domain.Field;
import org.pentaho.det.platform.domain.mapper.MapCdaToGoogleDataTable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CdaDataSource implements IDataSource {

  //region Properties
  //FIXME get host and port
  private static final String CDA_BASE_URL = "http://localhost:8080/pentaho/plugin/cda/api";
  private static final String AUTHENTICATION_PARAMS = "&userid=admin&password=password";

  private static final String LIST_QUERIES_ENDPOINT = CDA_BASE_URL + "/listQueries";
  private static final String LIST_PARAMETERS_ENDPOINT = CDA_BASE_URL + "/listParameters";
  private static final String DO_QUERY_ENDPOINT = CDA_BASE_URL + "/doQuery";

  public void setPath( String path ) {
    this.path = path;
  }
  public String getPath() {
    return this.path;
  }
  private String path;

  public void setParameters( Map<String, String> map ) {
    this.parameters = map;
  }
  public Map<String, String> getParameters() {
    return Collections.unmodifiableMap( this.parameters );
  }
  public void putParameter( String key, String value ) {
    this.parameters.put( key, value );
  }
  private Map<String, String> parameters;

  @Override
  public UUID getUUID() {
    return this.uuid;
  }
  private UUID uuid;

  @Override
  public IDataTable getData() {
    if ( this.dataTable == null ) {
      this.dataTable = this.doQuery();
    }

    return this.dataTable;
  }
  private IDataTable dataTable;

  public void setName( String name ) {
    this.name = name;
  }
  @Override
  public String getName() {
    return this.name;
  }
  private String name;

  private MapCdaToGoogleDataTable mapping;
  //endregion

  //region Constructor
  public CdaDataSource( String path, String id ) {
    this.mapping = MapCdaToGoogleDataTable.getInstance();
    this.uuid = UUID.randomUUID();

    this.setPath( path );
    this.setName( id );

    this.setParameters( new HashMap<String, String>() );
  }
  //endregion

  //region Methods
  public String listQueries() {
    String queryString = "?path=" + this.getPath() + AUTHENTICATION_PARAMS;

    String result = null;
    try {
      result = this.getDataFromServer( LIST_QUERIES_ENDPOINT + queryString );
    } catch ( Exception e ) {
      //log message
    }

    return result;
  }

  public String listParameters( String dataAccessId ) {
    String queryString = "?path=" + this.getPath() + "&dataAccessId=" + dataAccessId + AUTHENTICATION_PARAMS;

    String result = null;
    try {
      result = this.getDataFromServer( LIST_PARAMETERS_ENDPOINT + queryString );
    } catch ( Exception e ) {
      //log message
    }

    return result;
  }

  public IDataTable doQuery() {
    String parameters = this.buildParametersString();
    String queryString =
      "?path=" + this.getPath() + "&dataAccessId=" + this.getName() + parameters + AUTHENTICATION_PARAMS;

    IDataTable result = null;
    try {
      String data = this.getDataFromServer( DO_QUERY_ENDPOINT + queryString );
      result = this.parseData( new DataTable(), new JSONObject( data ) );
    } catch ( Exception e ) {
      //log message
    }

    return result;
  }

  private String getDataFromServer( String urlString ) throws Exception {
    StringBuilder sb = new StringBuilder();
    URL url = new URL( urlString );

    URLConnection urlConnection = url.openConnection();
    BufferedReader reader = new BufferedReader( new InputStreamReader( urlConnection.getInputStream() ) );
    String line;
    while ( ( line = reader.readLine() ) != null ) {
      sb.append( line );
    }
    reader.close();

    return sb.toString();
  }

  private IDataTable parseData( IDataTable dataTable, JSONObject data ) {
    int i, j;
    JSONArray resultset = data.getJSONArray( "resultset" );
    JSONArray metadata = data.getJSONArray( "metadata" );


    //Result Set -> Row Data Values
    for ( i = 0; i < resultset.length(); i++ ) {
      List<Object> dataList = new ArrayList<Object>();
      JSONArray row = resultset.getJSONArray( i );
      for ( j = 0; j < row.length(); j++ ) {
        dataList.add( row.get( j ) );
      }

      dataTable.getEntries().add( new DataTableEntry( dataList ) );
    }

    //Meta Data -> Header and Column Type
    List<IField> fields = dataTable.getFields();
    for ( i = 0; i < metadata.length(); i++ ) {
      JSONObject rowMeta = metadata.getJSONObject( i );

      String name = rowMeta.getString( "colName" );
      ColumnType colType = this.mapping.getDataType( rowMeta.getString( "colType" ) );

      fields.add( new Field( name, colType ) );
    }

    return dataTable;
  }

  private String buildParametersString() {
    String result = "";

    for ( Map.Entry<String, String> entry : this.getParameters().entrySet() ) {
      result += "&param" + entry.getKey() + "=" + entry.getValue();
    }

    return result;
  }
  //endregion


}
