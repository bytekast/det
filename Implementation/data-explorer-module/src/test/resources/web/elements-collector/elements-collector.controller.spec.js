/* globals _, requireConfigurations */
define(['pentaho/tests/scopedRequireManager', 'pentaho/tests/angularMocksUtils'], function(scopedRequireManager, angularMocksUtils) {
  'use strict';

  describe('Pentaho Visual Controller Test', function() {
    beforeEach(scopedRequireManager.createScopedRequire('this-module', {'paths': {'underscorejs': '/webjars/underscorejs/1.8.3/underscore'}}));
    beforeEach(angularMocksUtils.setup(false));

    beforeEach(angularMocksUtils.loadController('pentaho/det/DataExplorerModule/elements-collector/elements-collector.controller'));

    beforeEach(function() {
      this.mockElementList = [
        {'label': 1},
        {'label': 2},
        {'label': 3},
        {'label': 4},
        {'label': 5}
      ];
    });

    describe('Controller default value for ', function() {
      it('elements should be a empty array', function() {
        expect(this.$controller.elements).toEqual([]);
      });
    });

    afterEach(function() {
      delete this.mockElementList;
    });

    afterEach(angularMocksUtils.cleanup());
    afterEach(scopedRequireManager.destroyScopedRequire());
  });
});
