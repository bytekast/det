/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
    [
      'underscorejs',
      'angular',
      './app.controller',
      'text!./app.html',
      '../elements-collector/module',
      'service!IDataExplorerModulePlugin',
      '../pdi/transGraphService',
      'css!../data-explorer-module.css'
    ],
    function(_, angular, appController, appHtml, elementsModule, modulePlugins, transGraphService) {
      'use strict';

      var modulePluginsStates = _.chain(modulePlugins)
        .map(function(p) { return p.getStates(); })
        .flatten(true)
        .sortBy('order')
        .value();

      var module = {
        name: 'det-data-explorer-plugin',
        getStates: getStates
      };

      var states = [
        {
          name: module.name,
          url: '/' + module.name + '?transGraphId&hostPlatform',
          deepStateRedirect: true,
          template: appHtml,
          controller: appController,
          controllerAs: 'vm',
          label: 'Explore',
          order: 1,
          cssClasses: {
            'det-plugin': true,
            'det-data-explorer-plugin': true
          },
          evalRedirect: function() {
            if (modulePluginsStates.length > 0) {
              if (typeof modulePluginsStates[0].evalRedirect === 'function') {
                return modulePluginsStates[0].evalRedirect();
              }

              return modulePluginsStates[0].name;
            }
          },
          resolve: {
            datasource: function(datasourceService, datasourceId) {
              return datasourceService.getDataSource(datasourceId);
            },
            dropZone0: function() {
              return {
                header: 'Fields',
                description: ''
              };
            },
            transGraphId: function($stateParams) {
              return $stateParams['transGraphId'];
            },
            hostPlatform: function($stateParams) {
              return $stateParams['hostPlatform'];
            }
          },
          children: modulePluginsStates
        }
      ];

      activate();

      return module;

      /////////////////////

      function activate() {
        var deps = _.pluck(modulePlugins, 'name');
        deps.push(elementsModule.name);

        angular.module(module.name, deps)
          .constant('dataExplorerModulePlugins', modulePlugins)
          .constant('dataExplorerModulePluginsPluginsStates', modulePluginsStates)
          .factory('transGraphService', transGraphService);
      }

      function getStates() {
        return states;
      }
    }
);
