/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(['underscorejs'], function(_) {
  'use strict';

  ApplicationModuleAController.$inject = ['$scope', '$interval', 'dropZone0',
                                          'dataExplorerModulePluginsPluginsStates',
                                          'datasource','transGraphService', 'transGraphId', 'hostPlatform'];
  function ApplicationModuleAController($scope, $interval, dropZone0, modulePluginsStates,
                                        datasource, transGraphService, transGraphId, hostPlatform) {
    var vm = this;

    vm.pluginStates = modulePluginsStates;

    //drop zone directives data
    vm.dropZone0 = dropZone0;

    vm.datasource = datasource;

    vm.rowsNum = datasource.numberOfRows;
    vm.fields = datasource.getFields();
    vm.highlights = [];

    vm.transGraph = {
      id: transGraphId,
      isRunning: true,
      stop: stopTransGraph,
      getMoreRows: getMoreRows
    };

    vm.isPdiPlatform = isPdiPlatform;

    $scope.$on('receive-used-fields', function(event, used) {
      for (var i = 0, ic = vm.fields.length; i !== ic; ++i) {
        vm.highlights[i] = _.contains(used, vm.fields[i].uid);
      }
    });

    function stopTransGraph() {
      transGraphService.stopTransGraph(vm.transGraph.id).then(function() {
        vm.transGraph.isRunning = false;
      });
    }

    function getMoreRows() {
      transGraphService.getMoreRows(vm.transGraph.id).then(function() {
        vm.transGraph.isRunning = true;
        vm.datasource.refresh().then(function(ds) {
          $scope.$rootScope.$broadcast('datasource-changed', ds);
        });
      });
    }

    function isPdiPlatform() {
      return hostPlatform === 'pdi';
    }
  }

  return ApplicationModuleAController;
});
