/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
  [
      './elements-collector.controller',
      'text!./elements-collector.html'
  ],

  function(controller, template) {
    'use strict';

    ElementsCollectorDirective.$inject = [];
    function ElementsCollectorDirective() {
      var listId = 0;

      var directive = {
        restrict: 'E',
        template: template,
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,

        scope: {
          elements: '=*',
          highlights: '=*',
          action: '@',
          accept: '@',
          provide: '@',
          remove: '=',
          header: '=',
          description: '='
        },

        link: link
      };

      return directive;

      function link(scope, element) {
        scope.vm.listId = 'detElementsCollector_' + listId++;
        element.attr('id', scope.vm.listId);
      }
    }

    return ElementsCollectorDirective;
  }
);
