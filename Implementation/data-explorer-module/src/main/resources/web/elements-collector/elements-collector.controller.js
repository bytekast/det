/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(['underscorejs'], function(_) {
  'use strict';

  ElementsCollectorController.toElement = null;

  ElementsCollectorController.$inject = [];
  function ElementsCollectorController() {
    var vm = this;

    vm.header = vm.header || 'Collector\'s title';
    vm.elements = vm.elements || [];
    vm.description = vm.description || 'No fields to display.';

    vm.receiveField = receiveField;
    vm.dropped = dropped;
    vm.removeField = removeField;

    vm.clear = clear;

    ////////////////

    function clear() {
      vm.elements.length = 0;
    }

    function dropped(event, elementIndex) {
      if (event.dataTransfer.dropEffect === 'move' &&
          ElementsCollectorController.toElement !== vm.listId) {
        vm.elements.splice(elementIndex, 1);
      }

      ElementsCollectorController.toElement = null;
    }

    function receiveField(event, element, anchorIndex, position) {
      ElementsCollectorController.toElement = vm.listId;

      if (anchorIndex == null || element.list !== vm.listId || element.index !== anchorIndex) {
        var elementIndex = -1;

        if (element.list === vm.listId) {
          elementIndex = element.index;
        } else {
          elementIndex = _.findIndex(vm.elements, function(key) {
            return key.uid === element.data.uid;
          });
        }

        if (elementIndex !== -1) {
          vm.elements.splice(elementIndex, 1);

          if (anchorIndex > elementIndex) {
            --anchorIndex;
          }
        }

        if (anchorIndex != null) {
          vm.elements.splice(anchorIndex + (position === 'bottom' ? 1 : 0), 0, element.data);
        } else {
          vm.elements.push(element.data);
        }
      }
    }

    function removeField(elementIndex) {
      vm.elements.splice(elementIndex, 1);
    }
  }

  return ElementsCollectorController;
});
