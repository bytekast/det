/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(['underscorejs'], function(_) {
  'use strict';

  ModuleController.$inject = ['$scope', 'dropZone1', 'dropZone2', 'datasource'];
  function ModuleController($scope, dropZone1, dropZone2, datasource) {
    var vm = this;

    vm.dropZone1 = dropZone1;
    vm.dropZone2 = dropZone2;

    vm.groups = [];

    vm.ungroupedFields = [];
    vm.groupedFields = [];

    $scope.$watchCollection('$parent.vm.fields', copyFields);

    $scope.$on('emit-used-fields', emitUsedFields);

    $scope.$rootScope.$on('datasource-changed', refreshResultSet);

    var currentResultSet;

    function refreshResultSet() {
      if (currentResultSet) {
        currentResultSet.refresh().then(refreshData);
      }
    }

    function copyFields() {
      _.each($scope.$parent.vm.fields, function(f) {
        vm.ungroupedFields.push(f);
      });

      $scope.$watchCollection('vm.groupedFields', refreshData);
      $scope.$watchCollection('vm.ungroupedFields', reformatData);
    }

    function reformatData() {
      for (var i = vm.ungroupedFields.length - 1; i >= 0; --i) {
        var element = vm.ungroupedFields[i];

        if (_.find(vm.groupedFields, function(g) {
          return g.uid === element.uid;
        }) !== undefined) {
          refreshData();
          return;
        }
      }

      emitUsedFields();
    }

    function refreshData() {
      for (var i = vm.ungroupedFields.length - 1; i >= 0; --i) {
        var element = vm.ungroupedFields[i];

        if (_.find(vm.groupedFields, function(g) {
          return g.uid === element.uid;
        }) !== undefined) {
          vm.ungroupedFields.splice(i, 1);
        }
      }

      emitUsedFields();

      datasource.query({groupBy: vm.groupedFields}).then(function(resultSet) {
        currentResultSet = resultSet;

        vm.data = resultSet.getData();
        vm.groups = resultSet.getGroups();

        $scope.$digest();
      });
    }

    function emitUsedFields(event, id) {
      if (event === undefined || event.name === undefined || event != null && event.name === 'emit-used-fields' && id === 'det-data-table-component') {
        $scope.$emit('receive-used-fields', _.pluck(_.union(vm.ungroupedFields, vm.groupedFields), 'uid'));
      }
    }
  }

  return ModuleController;
});
