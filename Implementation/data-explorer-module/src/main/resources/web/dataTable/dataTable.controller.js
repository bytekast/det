/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(['underscorejs'], function(_) {
  'use strict';

  DataTableController.$inject = [];
  function DataTableController() {
    var vm = this;

    vm.receiveField = receiveField;
    vm.removeField = removeField;

    function receiveField(element, anchorIndex, position) {
      if (element.list !== vm.listId || element.index !== anchorIndex) {
        var elementIndex = -1;

        if (element.list === vm.listId) {
          elementIndex = element.index;
        } else {
          elementIndex = _.findIndex(vm.fields, function(key) {
            return key.uid === element.data.uid;
          });
        }

        if (elementIndex !== -1) {
          vm.fields.splice(elementIndex, 1);

          if (anchorIndex > elementIndex) {
            --anchorIndex;
          }
        }

        vm.fields.splice(anchorIndex + (position === 'right' ? 1 : 0), 0, element.data);
      }
    }

    function removeField(elementIndex) {
      vm.fields.splice(elementIndex, 1);
    }
  }

  return DataTableController;
});
