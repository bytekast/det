define(
  [
    './dataTable.controller',
    'text!./dataTable.html'
  ],
  function(controller, template) {
    'use strict';

    function detDataTableDirective() {
      var listId = 0;

      var directive = {
        template: template,
        restrict: 'E',
        scope: {
          fields: '=*',
          groups: '=*?',
          data: '=*'
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        link: link
      };

      return directive;

      function link(scope, element) {
        scope.vm.listId = 'detDataTable_' + listId++;
        element.attr('id', scope.vm.listId);
      }
    }

    return detDataTableDirective;

  }
);
