define(
  [
    './dataGroup.controller',
    'text!./dataGroup.html'
  ],
  function(controller, template) {
    'use strict';

    function detDataGroupDirective() {
      var directive = {
        template: template,
        require: '^^detDataTable',
        restrict: 'E',
        scope: {
          group: '=*?',
          data: '=*?'
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        link: link
      };

      return directive;

      function link(scope, element, attrs, parent) {
        scope.pvm = parent;
      }
    }

    return detDataGroupDirective;

  }
);
