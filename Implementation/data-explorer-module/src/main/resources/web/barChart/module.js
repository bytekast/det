/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
  [
    'angular',
    'text!./module.html',
    './module.controller',
    'angular-native-dragdrop'
  ],
  function(angular, moduleHtml, moduleController) {
    'use strict';

    var module = {
      name: 'det-bar-chart-component',
      getStates: getStates
    };

    var states = [
      {
        name: module.name,
        pluginName: module.name,
        url: '/' + module.name,
        deepStateRedirect: true,
        sticky: true,
        onReactivate: function($rootScope) {
          $rootScope.$broadcast('emit-used-fields', module.name);
        },
        views: {
          'det-bar-chart-component': {
            template: moduleHtml,
            controller: moduleController,
            controllerAs: 'vm'
          }
        },
        label: 'Bar Chart',
        order: 3,
        cssClasses: {
          'det-data-explorer-plugin-module': true,
          'det-bar-chart-component': true
        },
        resolve: {
          data: function(datasource) {
            return datasource.query();
          },
          dropZone1: function() {
            return {
              header: 'X Axis',
              description: 'Drag and drop here the fields that you want to see in the X Axis.'
            };
          },
          dropZone2: function() {
            return {
              header: 'Series',
              description: 'Drag and drop here the fields that you want to use as series'
            };
          },
          dropZone3: function() {
            return {
              header: 'Measures',
              description: 'Drag and drop here the fields that you want to use as measures.'
            };
          }
        }
      }
    ];

    activate();

    return module;

    function activate() {
      angular.module(module.name, []);
    }

    function getStates() {
      return states;
    }
  }
);
