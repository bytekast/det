/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(['underscorejs'], function(_) {
  'use strict';

  ModuleController.$inject = ['$scope', 'dropZone1', 'dropZone2', 'dropZone3', 'data'];
  function ModuleController($scope, dropZone1, dropZone2, dropZone3, data) {
    var vm = this;

    vm.dropZone1 = dropZone1;
    vm.dropZone2 = dropZone2;
    vm.dropZone3 = dropZone3;

    vm.rowsfields = [];
    vm.columnsfields = [];
    vm.measuresfields = [];

    vm.data = data;

    $scope.$watchCollection('vm.rowsfields', emitUsedFields);
    $scope.$watchCollection('vm.columnsfields', emitUsedFields);
    $scope.$watchCollection('vm.measuresfields', emitUsedFields);

    $scope.$on('emit-used-fields', emitUsedFields);

    function emitUsedFields(event, id) {
      if (event === undefined || event.name === undefined || event != null && event.name === 'emit-used-fields' && id === 'det-bar-chart-component') {
        $scope.$emit('receive-used-fields', _.pluck(_.union(vm.rowsfields, vm.columnsfields, vm.measuresfields), 'uid'));
      }
    }

    $scope.$rootScope.$on('datasource-changed', refreshResultSet);

    function refreshResultSet() {
      data.refresh().then(function() {
        $scope.$digest();
      });
    }
  }

  return ModuleController;
});
