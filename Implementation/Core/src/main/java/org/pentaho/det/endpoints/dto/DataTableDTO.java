/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

package org.pentaho.det.endpoints.dto;

import org.pentaho.det.api.domain.IDataTable;
import org.pentaho.det.api.domain.IDataTableEntry;
import org.pentaho.det.api.domain.IField;
import org.pentaho.det.api.endpoints.dto.IDataTableDTO;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public final class DataTableDTO implements IDataTableDTO {

  @XmlElement( name = "cols" )
  public List<ColumnDefinitionDTO> cols;

  @XmlElement( name = "rows" )
  public List<RowDTO> rows;

  @XmlElement( name = "p" )
  public Object p;

  public DataTableDTO() {
    this.cols = new ArrayList<>();
    this.rows = new ArrayList<>();
  }

  public DataTableDTO( IDataTable dataTable ) {
    this();

    if ( dataTable != null ) {
      for ( IField field : dataTable.getFields() ) {
        this.cols.add( new ColumnDefinitionDTO( field.getType(), field.getName() ) );
      }

      for ( IDataTableEntry dataTableEntry : dataTable.getEntries() ) {
        RowDTO rowDto = new RowDTO( dataTableEntry.getData().toArray() );
        this.rows.add( rowDto );
      }
    }

  }



}
