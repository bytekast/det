/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
    [
      'underscorejs',
      'angular',
      'service!IDetPlugin'
    ],
    function(_, angular, detPlugins) {
      'use strict';

      var module = {
        name: 'det-plugins'
      };

      activate();

      return module;

      function activate() {
        var deps = _.pluck(detPlugins, 'name');

        angular.module(module.name, deps)
          .constant('detPlugins', detPlugins)
          .constant('detPluginsStates',
                  _.chain(detPlugins)
                    .map(function(p) { return p.getStates(); })
                    .flatten(true)
                    .sortBy('order')
                    .value()
          );
      }
    }
);
