define(
  [
    'underscorejs',
    './app.controller',
    'text!./app.html'
  ],
  function(_, appController, appHtml) {
    'use strict';

    config.$inject = ['detAppModuleInfo', 'detPluginsStates', 'stateHelperProvider', '$urlRouterProvider', '$httpProvider'];
    function config(module, detPluginsStates, stateHelperProvider, $urlRouterProvider, $httpProvider) {
      var rootURL = '/' + module.name;

      var rootState = {
        name: module.name,
        url: rootURL + '?datasource-type&datasource-id',
        template: appHtml,
        controller: appController,
        controllerAs: 'vm',
        evalRedirect: function() {
          if (detPluginsStates.length > 0) {
            if (typeof detPluginsStates[0].evalRedirect === 'function') {
              return detPluginsStates[0].evalRedirect();
            }

            return detPluginsStates[0].name;
          }
        },
        resolve: {
          datasourceType: function($stateParams) {
            return $stateParams['datasource-type'] || 'pdi-datasource';
          },
          datasourceService: function(datasourceType, detDatasourceResolver) {
            return detDatasourceResolver.resolve(datasourceType);
          },
          datasources: function(datasourceService) {
            return datasourceService.getDataSources();
          },
          datasourceId: function($stateParams, datasourceService, datasources) {
            var uuid = null;
            if ($stateParams['datasource-id']) {
              uuid = $stateParams['datasource-id'];
            } else if (datasources.length > 0) {
              uuid = datasources[0].uuid;
            }

            return uuid;
          }
        },
        children: detPluginsStates
      };

      stateHelperProvider.state(rootState);

      // For any unmatched url, send to root url
      $urlRouterProvider.otherwise(rootURL);

      $httpProvider.defaults.withCredentials = true;
    }

    return config;
  }
);
