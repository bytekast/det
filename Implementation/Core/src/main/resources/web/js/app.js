/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
    [
      'underscorejs',
      'angular',

      './app.config',

      '../datasources/dataSourceResolverService',

      '../datasources/pdi/module',
      '../plugins/module',

      '../visual/pentaho-visual/pentaho-visual.directive',

      'css!../lib/bootstrap/3.3.5/css/bootstrap.css',
      'css!../css/pentaho-det-core.css',

      'angular-ui-router',
      'angular-ui-router.stateHelper',
      'ui-router-extras',
      'angular-resource',

      'bootstrap',
      'modernizr'
    ],
    function(
       _,
      angular,
      config,
      datasourceResolverService,
      pdiPreviewDataSourceModule,
      detPluginsModule,
      pentahoVisualDirective) {

      'use strict';

      var module = {
        name: 'det-app',
        init: detAppInit
      };

      activate();

      return module;

      ////////////////////

      function activate() {
        var deps = [
          'ui.router',
          'ui.router.stateHelper',
          'ct.ui.router.extras.dsr',
          'ct.ui.router.extras.sticky',
          'ngResource',
          pdiPreviewDataSourceModule.name,
          detPluginsModule.name
        ];

        angular.module(module.name, deps)
          .constant('detAppModuleInfo', module)
          .config(config)
          .directive('detPentahoVisual', pentahoVisualDirective)
          .service('detDatasourceResolver', datasourceResolverService)
          .run(detAppRun);
      }

      function detAppInit(element) {
        angular.element(element).ready(function() {
          angular.bootstrap(element, [module.name]);
        });
      }

      function detAppRun($rootScope, $state) {
        $rootScope['$state'] = $state;
        $rootScope['$rootScope'] = $rootScope;

        $rootScope.$on('$stateChangeStart',
          function(e, toState, toParams) {
            if (typeof toState.evalRedirect === 'function') {
              var dest = toState.evalRedirect(toParams);

              if (typeof dest === 'string') {
                e.preventDefault();
                $state.go(dest, toParams);
                return false;
              }
            }
          });
        /*
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
          console.log(event, error);
          return $state.go(module.name);
        });
        */
      }
    }
);
