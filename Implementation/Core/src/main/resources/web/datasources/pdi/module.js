/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
    [
      'angular',
      './dataSourceService',
      'angular-resource'
    ],
    function(angular, dataSourceService) {
      'use strict';

      var module = {
        name: 'pdi-datasource'
      };

      activate();

      return module;

      function activate() {
        angular.module(module.name, [])
          .factory('detEndpoint', function($resource) {
            //FIXME switch OSGI_CONTEXT_PATH to angular constant
            var baseUrl = OSGI_CONTEXT_PATH + 'cxf/DataExplorerTool/det';

            return $resource(baseUrl + '/dataSources/:uuid',
                {
                  uuid: '@uuid'
                },
                {
                  getData: {
                    method: 'GET',
                    url: baseUrl + '/dataSources/:uuid/data'
                  }
                }
            );
          })
          .factory(module.name + '.dataSourceService', dataSourceService);
      }
    }
);
