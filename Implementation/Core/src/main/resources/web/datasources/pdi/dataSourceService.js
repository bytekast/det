/*
 * Copyright 2002 - 2015 Webdetails, a Pentaho company. All rights reserved.
 *
 * This software was developed by Webdetails and is provided under the terms
 * of the Mozilla Public License, Version 2.0, or any later version. You may not use
 * this file except in compliance with the license. If you need a copy of the license,
 * please go to http://mozilla.org/MPL/2.0/. The Initial Developer is Webdetails.
 *
 * Software distributed under the Mozilla Public License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. Please refer to
 * the license for the specific language governing your rights and limitations.
 */

define(
    [
      'underscorejs'
    ],
    function(_) {
      'use strict';

      DataSourceService.$inject = ['detEndpoint', '$q'];
      function DataSourceService(detEndpoint, $q) {
        var service = {
          getDataSources: getDataSources,
          getDataSource: getDataSource
        };

        return service;

        ////////////////

        function getDataSources() {
          var maxRequests = 2;
          //FIXME: multiple requests are being made because there is
          // an issue with cxf services and jackson serializer where the first request always fails
          function getDataSourcesAux(executedRequests) {
            return detEndpoint.query().$promise.then(
                function getDataSourcesSuccessHandler(response) {
                  return response;
                },
                function getDataSourcesFailureHandler() {
                  if (executedRequests === maxRequests) {
                    return $q.reject('Unable to get datasources');
                  }
                  return getDataSourcesAux(executedRequests + 1);
                });
          }

          return getDataSourcesAux(1);
        }

        function getDataSource(uuid) {
          return detEndpoint.get({uuid: uuid}).$promise.then(function(info) {
            var fields = [];
            var rows = [];

            info.query = query;
            info.getFields = getFields;

            info.refresh = refresh;

            return new Promise(function(resolve, reject) {
              info.resultSet = detEndpoint.getData({uuid: uuid}).$promise;

              info.resultSet.then(function(d) {
                var fieldUid = 0;
                _.each(d.cols, function(field) {
                  field.uid = fieldUid++;
                  field.id = 'gen_' + fieldUid;
                  field.name = 'gen_' + fieldUid;
                  fields.push(field);
                });

                d.model = d.cols;
                d.cols = null;

                updateRows(d);

                resolve(info);
              }, function(e) {
                reject(e);
              });
            });

            function refresh() {
              return new Promise(function(resolve, reject) {
                doRequest(10).then(function(res) {
                  if (res) {
                    resolve(info);
                  } else {
                    reject();
                  }
                }, function() {
                  reject();
                });
              });
            }

            function doRequest(timeout) {
              return new Promise(function(resolve, reject) {
                detEndpoint.getData({uuid: uuid}).$promise.then(function(d) {
                  if (d.rows.length > info.numberOfRows) {
                    updateRows(d);
                    resolve(true);
                  } else {
                    if (timeout > 0) {
                      _.delay(function() {
                        resolve(doRequest(--timeout));
                      }, 750);
                    } else {
                      reject(false);
                    }
                  }
                });
              });
            }

            function updateRows(d) {
              rows.length = 0;
              Array.prototype.push.apply(rows, d.rows);
              info.numberOfRows = rows.length;
            }

            function query(queryParams) {
              var data = [];
              var numberOfRows = 0;

              var groups = [];
              var groupsIndex = [];

              var resultSet = {};

              resultSet.timestamp = new Date();

              resultSet.getFields = getFields;

              resultSet.getData = getData;
              resultSet.getGroups = getGroups;
              resultSet.getNumberOfRows = getNumberOfRows;

              resultSet.refresh = rsRefresh;

              resultSet.model = [];
              Array.prototype.push.apply(resultSet.model, fields);

              return processData(rows);

              function rsRefresh() {
                return processData(rows);
              }

              function getData() {
                return resultSet.rows;
              }

              function getGroups() {
                return groups;
              }

              function getNumberOfRows() {
                return numberOfRows;
              }

              function processData(rows) {
                return new Promise(function(resolve) {
                  data.length = 0;
                  numberOfRows = 0;

                  groups.length = 0;
                  groupsIndex.length = 0;

                  var processed;
                  if (queryParams != null && queryParams.groupBy != null && queryParams.groupBy.length > 0) {
                    processed = _.groupBy(rows, function(value) {
                      var group = {};

                      _.each(queryParams.groupBy, function(groupedField) {
                        group[groupedField.label] = value.c[groupedField.uid] != null ?
                                                        value.c[groupedField.uid].v :
                                                        null;
                      });

                      var key = JSON.stringify(group);

                      var groupId = _.indexOf(groupsIndex, key);
                      if (groupId === -1) {
                        groupsIndex.push(key);
                        groupId = groupsIndex.length - 1;

                        groups.push(group);
                      }

                      return groupId;
                    });

                    data = processed;
                  } else {
                    Array.prototype.push.apply(data, rows);
                  }

                  // TODO: Add also number of rows by group
                  numberOfRows = rows.length;

                  resultSet.rows = data;
                  resultSet.timestamp = new Date();

                  resolve(resultSet);
                });
              }
            }

            function getFields() {
              return fields;
            }
          });
        }
      }

      return DataSourceService;
    }
);
