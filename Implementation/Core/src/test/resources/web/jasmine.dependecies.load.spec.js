define(['angular'], function() {
  describe('Checking test dependencies loaded by jasmine maven plugin', function() {
    it('angular.js is loaded and is 1.4 branch', function() {
      expect(angular).not.toBe(undefined);
      expect(angular.version.major).toBe(1);
      expect(angular.version.minor).toBe(4);
    });
  });
});
