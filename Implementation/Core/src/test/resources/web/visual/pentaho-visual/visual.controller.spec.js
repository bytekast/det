/* globals _, requireConfigurations */
define(['pentaho/tests/scopedRequireManager', 'pentaho/tests/angularMocksUtils'], function(scopedRequireManager, angularMocksUtils) {
  'use strict';

  describe('Pentaho Visual Controller Test', function() {
    beforeEach(scopedRequireManager.createScopedRequire('this-module'));
    beforeEach(angularMocksUtils.setup(false));

    beforeEach(angularMocksUtils.loadController('pentaho/det/visual/pentaho-visual/pentaho-visual.controller'));

    it('controller title should be PentahoVisualController', function(done) {
      expect(this.$controller.title).toBe('PentahoVisualController');

      done();
    });

    it('controller requirements should be empty', function(done) {
      expect(this.$controller.requirements).toEqual({});

      done();
    });

    afterEach(angularMocksUtils.cleanup());
    afterEach(scopedRequireManager.destroyScopedRequire());
  });
});
