/* globals _, requireConfigurations */
define(['pentaho/tests/scopedRequireManager', 'pentaho/tests/angularMocksUtils'], function(scopedRequireManager, angularMocksUtils) {
  'use strict';

  describe('App Controller Test', function() {
    beforeEach(scopedRequireManager.createScopedRequire('this-module'));
    beforeEach(angularMocksUtils.setup(true));

    beforeEach(angularMocksUtils.loadController('pentaho/det/js/app.controller', function() {
      return [this.$state, this.$scope];
    }));

    it('controller title should be ApplicationController', function(done) {
      expect(this.$controller.title).toBe('ApplicationController');

      done();
    });

    it('state should change when datasourceId changes', function(done) {
      var newDatasourceId = 'AAA';

      this.$watchSpies['vm.datasourceId'](newDatasourceId);

      expect(this.$state.go).toHaveBeenCalledWith('test', Object({ 'datasource-id': newDatasourceId }));

      done();
    });

    afterEach(angularMocksUtils.cleanup());
    afterEach(scopedRequireManager.destroyScopedRequire());
  });

  function newController(done, ControllerClass) {
    console.log(arguments);
    this.$controller = new ControllerClass(this.$state, this.$scope);
    this.$scope.vm = this.$controller;

    done();
  }
});
