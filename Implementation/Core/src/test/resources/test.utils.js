/* globals requireConfigurations */

define('pentaho/tests/scopedRequireManager', [], function() {
  'use strict';

  var scopedRequireManager = {
    createScopedRequire: function() {
      var configs = arguments;
      return function(done) {
        _.bind(createScopedRequire, this, configs, done)();
      };
    },
    destroyScopedRequire: function() {
      return function(done) {
        _.bind(destroyScopedRequire, this, done)();
      };
    },
  };

  return scopedRequireManager;

  function createScopedRequire(configs, done) {
    this.require = generateRequire.apply(this, configs);

    if (typeof done === 'function') {
      done();
    }
  }

  function destroyScopedRequire(done) {
    this.require.undef();
    delete this.require;

    if (typeof done === 'function') {
      done();
    }
  }

  function generateRequire() {
    var requireconfig = {'shim': {},'paths': {},'map': {'*': {}},'config': {'service': {}}, 'context': 'test_' + new Date().getTime()};

    _.each(arguments, function(argument) {
      var config;
      if (typeof argument === 'string') {
        config = requireConfigurations[argument];
      } else {
        config = argument;
      }

      if (config.shim) {
        _.extendOwn(requireconfig.shim, config.shim);
      }

      if (config.paths) {
        _.extendOwn(requireconfig.paths, config.paths);
      }

      if (config.map) {
        _.each(config.map, function(path) {
          if (!requireconfig.map[path]) {
            requireconfig.map[path] = {};
          }

          _.extendOwn(requireconfig.map[path], config.map[path]);
        });
      }

      if (config.config && config.config.service) {
        _.extendOwn(requireconfig.config.service, config.config.service);
      }
    });

    return require.config(requireconfig);
  }
});

define('pentaho/tests/angularMocksUtils', ['angular-mocks'], function() {
  'use strict';

  var angularMocksUtils = {
    setup: function(interceptWatches) {
      return function(done) {
        _.bind(setup, this, interceptWatches, done)();
      };
    },
    cleanup: function() {
      return function(done) {
        _.bind(cleanup, this, done)();
      };
    },
    loadController: function(controllerId, args) {
      return function(done) {
        _.bind(loadController, this, controllerId, args, done)();
      };
    }
  };

  return angularMocksUtils;

  function setup(interceptWatches, done) {
    angular.mock.inject(function(_$rootScope_) {
      this.$rootScope = _$rootScope_;
    });

    this.$state = jasmine.createSpyObj('$state', ['go']);
    this.$state.current = {name: 'test'};

    this.$scope = this.$rootScope.$new();

    if(interceptWatches) {
      this.$watchSpies = {};

      spyOn(this.$scope, '$watch').and.callFake(_.bind(interceptCallback, this));
    }

    if (typeof done === 'function') {
      done();
    }
  }

  function cleanup(done) {
    delete this.$state;
    delete this.$scope;
    delete this.$rootScope;
    delete this.$watchSpies;
    delete this.$controller;

    if (typeof done === 'function') {
      done();
    }
  }

  function loadController(controllerId, args, done) {
    this.require([controllerId], _.bind(newController, this, done, args));
  }

  function newController(done, args, ControllerClass) {
    //this.$scope.vm = {};
    var args = args == null ? [] : (typeof args === 'function' ? _.bind(args, this)() : args);
    this.$controller = new (Function.prototype.bind.apply(ControllerClass, _.union([null], args)));

    if (typeof done === 'function') {
      done();
    }
  }

  function interceptCallback(expression, callback) {
    this.$watchSpies[expression] = callback;
  }
});
